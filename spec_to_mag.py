#!/usr/bin/env python

import pyphot
import numpy as np
import matplotlib.pyplot as plt
import click


def plot(filtname):
    lib = pyphot.get_library()
    filt = lib[filtname]
    wave = filt.wavelength.magnitude
    trans = filt.transmit
    plt.plot(wave, trans)
    plt.show()


def get_mag(wave, flux, filtername):
    lib = pyphot.get_library()
    filt = lib[filtername]
    sumflux = filt.get_flux(wave, flux)
    mag = pyphot.fluxToMag(sumflux) - filt.Vega_zero_mag
    return mag


def print_filter():
    lib = pyphot.get_library()
    filtlst = sorted(lib.content)
    for name in filtlst:
        print(name)


@click.command()
@click.option('--list', is_flag=True, default=False, help='list all the filter')
@click.option('--show_filter', default='NULL', help='show the transparency figure of a filter')
def main(list, show_filter):
    if list is True:
        print_filter()
    if show_filter is not 'NULL':
        plot(show_filter)


if __name__ == '__main__':
    main()
